import controller.Loggin;
import model.userInfoModels.UserLoginInfo;

import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws SQLException {
        UserLoginInfo ui =new UserLoginInfo();
        ui.getLoginInfo();
        Loggin loggin = new Loggin();
        loggin.checkIfUserPresentInDB();
        }
    }
