package model.userInfoModels;

import controller.ConnectToDB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserLoginInfo {
    private String userLogin;
    private String userPassword;

    public void getLoginInfo() throws SQLException {
        Connection connection = ConnectToDB.openConnection();
        ResultSet resultSet = null;
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM estate_agency.user_login_info");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                if (!resultSet.next()) break;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                setUserLogin(resultSet.getString("user_login"));
                setUserPassword(resultSet.getString("user_password"));
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public String getUserPassword() {
        return userPassword;
    }
}
