package model.userInfoModels;

import controller.ConnectToDB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserContractInfo {
    private String passportNumber;
    private int registrationNumber;
    private String registrationAddress;
    private String cardNumber;

    public void getContractInfo() throws SQLException {
        Connection connection = ConnectToDB.openConnection();
        ResultSet resultSet = null;
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM estate_agency.user_contract_info");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                if (!resultSet.next()) break;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                setPassportNumber(resultSet.getString("user_passport_number"));
                setRegistrationNumber(resultSet.getInt("user_registration_number"));
                setRegistrationAddress (resultSet.getString("user_registration_address"));
                setCardNumber(resultSet.getString("user_card_number"));

                System.out.println(passportNumber + registrationNumber +
                        registrationAddress + cardNumber);
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public void setRegistrationNumber(int registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public void setRegistrationAddress(String registrationAddress) {
        this.registrationAddress = registrationAddress;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
