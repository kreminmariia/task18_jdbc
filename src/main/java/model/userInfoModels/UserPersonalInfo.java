package model.userInfoModels;

import controller.ConnectToDB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserPersonalInfo {
    private String firstName;
    private String secondName;
    private String userBirthday;
    private String userPhoneNumber;
    private String userEmail;

    public void getPersonalInfo() throws SQLException {
        Connection connection = ConnectToDB.openConnection();
        ResultSet resultSet = null;
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM estate_agency.user_personal_info");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                if (!resultSet.next()) break;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                UserPersonalInfo user = new UserPersonalInfo();
                setFirstName(resultSet.getString("user_firstname"));
                setSecondName(resultSet.getString("user_secondname"));
                setUserBirthday (resultSet.getString("user_birthday"));
                setUserPhoneNumber (resultSet.getString("user_phone"));
                setUserEmail (resultSet.getString("user_email"));

                System.out.println(firstName + secondName + userBirthday + userPhoneNumber + userEmail);
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    private void setUserBirthday(String userBirthday) {
        this.userBirthday = userBirthday;
    }

    private void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    private void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
