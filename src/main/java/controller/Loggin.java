package controller;

import org.apache.log4j.Logger;
import view.Scanner;

import java.sql.*;


public class Loggin {
    private String databaseUsername = "";
    private String databasePassword = "";
    int userID=0;
    private static final Logger log = Logger.getLogger(Loggin.class.getName());
    Connection connection = ConnectToDB.openConnection();

    public int checkIfUserPresentInDB() throws SQLException {
        String userInsertedLogin = new Scanner().readStringUserInput();
        String userInsertedPassword = new Scanner().readStringUserInput();
        String SQL = "SELECT * FROM user_login_info WHERE user_login=? and user_password=?";
        PreparedStatement statement = connection.prepareStatement(SQL);
        statement.setString(1, userInsertedLogin);
        statement.setString(2, userInsertedPassword);

        ResultSet rs = statement.executeQuery(SQL);

        while (rs.next()) {
            databaseUsername = rs.getString("users_name");
            databasePassword = rs.getString("users_password");
        }

        if (userInsertedLogin.equals(databaseUsername) &&
                userInsertedPassword.equals(databasePassword)) {
            userID = rs.getInt(String.valueOf(connection.prepareStatement("SELECT id from user_login_info where user_login=? and user_password=?")));
            log.trace("Ви успішно залоговані");
        } else {
            log.error("Ви невірно вказали логін чи пароль. Спробуйте ще раз");
            checkIfUserPresentInDB();
        }
        return userID;
    }
}
