package controller;

import org.apache.log4j.Logger;

import java.sql.*;

public class ConnectToDB {

    private static final Logger log = Logger.getLogger(ConnectToDB.class.getName());

    private static final String DATABASE_URL="jdbc:mysql://localhost:3306/estate_agency?serverTimezone=UTC&useSSL=false&allowPublicKeyRetrieval=true";
    private static final String USER ="root";
    private static final String PASSWORD = "12345678";

    public static Connection openConnection() {
        Connection connection=null;
        try {
            Driver driver = new com.mysql.cj.jdbc.Driver();
            DriverManager.registerDriver(driver);

            connection = DriverManager.getConnection(DATABASE_URL,USER,PASSWORD);
            if(!connection.isClosed()){
                log.trace("Ми успішно підключились до бази даних \n");
            }
        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
        }
        return connection;
    }

    private ConnectToDB() {
    }
}
