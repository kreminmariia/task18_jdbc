package view;

public class Scanner {
    java.util.Scanner scanner = new java.util.Scanner(System.in);

    public int readIntUserInput(){
        int choice= scanner.nextInt();
        return choice;
    }
    public String readStringUserInput(){
       String choice= scanner.nextLine();
       return choice;
    }
}
